import { IntroduceStyles } from './Introduce.styles.tw'
import React from 'react'

export default function Introduce(): JSX.Element {
  return (
    <IntroduceStyles className={'introduce'}>
      <h3 className={'introduce-title'}>Давайте познакомимся</h3>
      <p className={'introduce-info'}>Меня зовут Елена и я являюсь профессиональным переводчиком китайского, английского, училась в Китае и готова организовать перевод любой информации</p>
    </IntroduceStyles>
  )
}
