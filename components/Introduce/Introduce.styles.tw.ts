import tw from 'twin.macro'
import styled from 'styled-components'

export const IntroduceStyles = styled.div`
  ${tw`relative items-center px-[20px] m-[0 auto] mb-[70px] md:mb-[100px] lg:mb-[270px] lg:flex lg:w-[1250px]`},
  .introduce {
    &:before {
      ${tw`lg:w-[2px] lg:h-[206px] lg:left-2/4`}
      position: absolute;
      content: "";
      border: 1px solid #6A6A6A;
      width: 100%;
      //top: 30%;
      left: 0;
    }
  }
  .introduce-title {
    ${tw`mb-[50px] block m-[0 auto] md:text-5xl lg:text-6xl lg:w-[50%] md:pt-[70px]`}
    color: rgba(255, 255, 255, .7);
    text-align: center;
    font-size: 25px;
    font-weight: 700;
  }
  .introduce-info {
    ${tw`lg:w-[50%]`}
    text-align: center;
    ${tw``}
  }
`
