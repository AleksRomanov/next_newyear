import { AboutStyles } from './About.styles.tw'
import { Button } from 'antd'
import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import Image from 'next/image'

export default function About({ title }): JSX.Element {
  return (
    <AboutStyles className={'about'}>
      <div className={'waviy'}>
        <span style={{ '--i': 1 } as React.CSSProperties}>{title[0]}</span>
        <span style={{ '--i': 2 } as React.CSSProperties}>{title[1]}</span>
        <span style={{ '--i': 3 } as React.CSSProperties}>{title[2]}</span>
        <span style={{ '--i': 4 } as React.CSSProperties}>{title[3]}</span>
        <span style={{ '--i': 5 } as React.CSSProperties}>{title[4]}</span>
        <span style={{ '--i': 6 } as React.CSSProperties}>{title[5]}</span>
      </div>
      {/*<h1 className="about-title">{title}</h1>*/}
      <div className="about-swiper">
        {/* eslint-disable-next-line no-console */}
        <Swiper className={'swiper-list'} spaceBetween={0} slidesPerView={1} onSlideChange={() => console.log('slide change')} onSwiper={(swiper) => console.log(swiper)}>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/1.jpeg'} alt={'Slide 1'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/2.jpeg'} alt={'Slide 2'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/3.jpeg'} alt={'Slide 3'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/4.jpeg'} alt={'Slide 4'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/5.jpeg'} alt={'Slide 5'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/6.jpeg'} alt={'Slide 6'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/7.jpeg'} alt={'Slide 7'} width={1920} height={360} />
          </SwiperSlide>
          <SwiperSlide className={'swiper-item'}>
            <Image src={'/images/about-swiper/8.jpeg'} alt={'Slide 8'} width={1920} height={360} />
          </SwiperSlide>
        </Swiper>
      </div>
      <Button className={'about-button'}>Button</Button>
    </AboutStyles>
  )
}
