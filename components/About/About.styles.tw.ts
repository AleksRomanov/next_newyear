import tw from 'twin.macro'
import styled from 'styled-components'

export const AboutStyles = styled.div`
  ${tw`relative`}
  .about-title {
    font-size: 78px;
    font-weight: 700;
    position: absolute;
    margin-left: 0;
    margin-right: 0;
    top: 33%;
    left: 0;
    right: 0;
    text-align: center;
    z-index: 2;
    color: rgba(255, 255, 255, .4);
  }

  .about-swiper {
    ${tw`mb-10`}
  }

  .swiper-item {
    ${tw`h-[580px] mr-0`}
  }

  .about-link {
    ${tw`flex justify-around px-20 my-10`}
  }

  .about-button {
    ${tw`text-white hover:bg-blue-300 hover:text-black mb-10 m-[0 auto] block`}
  }

  .waviy {
    font-size: 78px;
    font-weight: 700;
    position: absolute;
    margin-left: 0;
    margin-right: 0;
    top: 33%;
    left: 0;
    right: 0;
    text-align: center;
    z-index: 2;
    -webkit-box-reflect: below -20px linear-gradient(transparent, rgba(0, 0, 0, .2));
  }

  .waviy span {
    font-family: 'Montserrat-Regular', sans-serif;
    position: relative;
    display: inline-block;
    color: rgba(255, 255, 255, .4);
    letter-spacing: 1em;
    text-transform: uppercase;
    animation: waviy 1s infinite;
    animation-delay: calc(.1s * var(--i));

  }

  @keyframes waviy {
    0%, 40%, 100% {
      transform: translateY(0)
    }
    20% {
      transform: translateY(-20px)
    }
  }
`
