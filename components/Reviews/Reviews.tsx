import { ReviewsStyles } from './Reviews.styles.tw'
import React from 'react'
import 'swiper/css'
import { Swiper, SwiperSlide } from 'swiper/react'
import Image from 'next/image'

export default function Reviews(): JSX.Element {
  return (
    <ReviewsStyles className={'reviews'}>
      <h1 className={'reviews-title'}>Благодарности клиентов</h1>
      <div className="reviews-wrapper">
        <div className="reviews-swiper">
          {/* eslint-disable-next-line no-console */}
          <Swiper
            className={'reviews-list'}
            spaceBetween={30}
            slidesPerView={1}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
            breakpoints={{
              768: {
                spaceBetween: 30,
                slidesPerView: 2,
              },
              1250: {
                spaceBetween: 30,
                slidesPerView: 3,
              },
            }}
          >
            <SwiperSlide className={'reviews-list__item'}>
              <div className="reviews-list-item__ava"></div>
              <p className="reviews-list-item__title">Иванов Петр Борисович</p>
              <span className="reviews-list-item__content">Текстовка с благодарностью и отзывом клиента</span>
              <Image src={'/images/about-swiper/1.jpeg'} alt={'Slide 1'} width={368} height={400} />
            </SwiperSlide>
            <SwiperSlide className={'reviews-list__item'}>
              <div className="reviews-list-item__ava"></div>
              <p className="reviews-list-item__title">Иванов Петр Борисович</p>
              <span className="reviews-list-item__content">Текстовка с благодарностью и отзывом клиента</span>
              <Image src={'/images/about-swiper/2.jpeg'} alt={'Slide 1'} width={368} height={400} />
            </SwiperSlide>
            <SwiperSlide className={'reviews-list__item'}>
              <div className="reviews-list-item__ava"></div>
              <p className="reviews-list-item__title">Иванов Петр Борисович</p>
              <span className="reviews-list-item__content">Текстовка с благодарностью и отзывом клиента</span>
              <Image src={'/images/about-swiper/3.jpeg'} alt={'Slide 1'} width={368} height={400} />
            </SwiperSlide>{' '}
            <SwiperSlide className={'reviews-list__item'}>
              <div className="reviews-list-item__ava"></div>
              <p className="reviews-list-item__title">Иванов Петр Борисович</p>
              <span className="reviews-list-item__content">Текстовка с благодарностью и отзывом клиента</span>
              <Image src={'/images/about-swiper/3.jpeg'} alt={'Slide 1'} width={368} height={400} />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </ReviewsStyles>
  )
}
