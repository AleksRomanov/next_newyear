import tw from 'twin.macro'
import styled from 'styled-components'

export const ReviewsStyles = styled.div`
  ${tw`relative px-[20px] mb-[30px] lg:mb-[100px] md:mb-[100px]`}
  .reviews-swiper {
    //width: 90%;
    width: 100%;
    //display: flex;
    //justify-content: center;
    //align-items: center;
  }
  .reviews-title {
    ${tw`lg:text-6xl lg:mb-[100px] md:text-5xl md:mb-[100px]`}
    text-align: center;
    margin-bottom: 30px;
    font-size: 25px;
    //line-height: 77px;
    font-weight: 700;
    z-index: 2;
    color: rgba(255, 255, 255, .7);
    //width: 374px;
  }

  .reviews-list {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .reviews-list-item {
    ${tw` text-white  hover:bg-stone-800  text-xl`}
    
  }
  
  .reviews-swiper {
    ${tw`mb-10`}
  }
  
  .swiper-backface-hidden .swiper-slide {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
`
