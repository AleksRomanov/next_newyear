import Head from 'next/head'
import { ContainerStyled } from './LayoutCommon.styles.tw'
// import FooterNav from './common/Teaser/FooterNav'
import Header from './common/Header/Header'
import Footer from './common/Footer/Footer'

export default function MainLayout({ children, title = 'Aleks App' }) {
  return (
    <ContainerStyled isMobileDevice={false}>
      <Head>
        <title>{title} | Китайский перевод</title>
      </Head>
      <Header />
      <main>{children}</main>
      <Footer />
    </ContainerStyled>
  )
}
