import tw from 'twin.macro'
import styled from 'styled-components'

interface ContainerProps {
  isMobileDevice?: boolean
}

export const ContainerStyled = styled.div(({isMobileDevice}: ContainerProps) => [
    tw`mx-auto flex flex-col min-h-screen text-white bg-black`,
    isMobileDevice && tw`hidden`,
  ]
)
