import React from 'react'
import 'swiper/css'
import { TeamStyles } from './Team.styles.tw'
// import PrevArrow from '../../public/images/svg/arrow-prev.svg'
// import NextArrow from '../../public/images/svg/arrow-next.svg'
// import Image from 'next/image'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Button } from 'antd'
import Helen from '../../public/images/team/helen.svg'
import Aleks from '../../public/images/team/aleks.svg'

export default function Team(): JSX.Element {
  return (
    <TeamStyles className={'team'}>
      <h1>Наша команда</h1>
      <div className="team-wrapper">
        {/*<PrevArrow className={'swiper-button-prev team-swiper__swiper-button-prev'} />*/}
        {/*<NextArrow className={'swiper-button-next team-swiper__swiper-button-next'} />*/}
        <div className="team-swiper">
          {/* eslint-disable-next-line no-console */}
          <Swiper
            className={'team-list'}
            spaceBetween={30}
            slidesPerView={1}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
            breakpoints={{
              768: {
                spaceBetween: 30,
                slidesPerView: 2,
              },
              1250: {
                spaceBetween: 30,
                slidesPerView: 2,
              },
            }}
          >
            <SwiperSlide className={'team-list-item'}>
              <div className={'team-list-item__head'}>
                <div>
                  <p className="team-list-item__title">Елена Романова</p>
                  <span>Переводчик</span>
                </div>
                <div className="team-list-item__ava">
                  <Helen />
                </div>
              </div>
              <div className="team-list-item__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, aspernatur debitis deleniti, dolor dolores evenie</div>
              <Button className={'team-list__item-btn'} type="primary">
                Подробнее
              </Button>
            </SwiperSlide>
            <SwiperSlide className={'team-list-item'}>
              <div className={'team-list-item__head'}>
                <div>
                  <p className="team-list-item__title">Алексей Романов</p>
                  <span>Разработчик</span>
                </div>
                <div className="team-list-item__ava">
                  <Aleks />
                </div>
              </div>
              <div className="team-list-item__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, aspernatur debitis deleniti, dolor dolores evenie</div>
              <Button className={'team-list__item-btn'} type="primary">
                Подробнее
              </Button>
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </TeamStyles>
  )
}
