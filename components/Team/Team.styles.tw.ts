import tw from 'twin.macro'
import styled from 'styled-components'

export const TeamStyles = styled.div`
  ${tw`relative m-[0 auto] mb-[40px] px-[20px] lg:w-[1250px]`}
  .team-list {
    &__item-btn {
      ${tw`block m-[0 auto] bg-amber-400 hover:bg-amber-700 text-black rounded-2xl w-[121px] h-[43px]`}
    }
  }

  .team-list-item {
    ${tw` text-white p-[20px] hover:bg-stone-800`}
    background-color: rgba(29, 29, 29, .1);
    border: 1.4px solid rgba(255, 255, 255, 0.1);
    border-radius: 14px;
    margin-right: 0;

    &__head {
      display: flex;
      justify-content: space-between;
      margin-bottom: 30px;
    }
    &__title {
      margin-top: 20px;
    }
    &__content {
      margin-bottom: 30px;  
    }
  }

  h1 {
    ${tw`lg:text-6xl lg:mb-[100px] md:text-5xl md:mb-[100px]`}
    margin-bottom: 30px;
    text-align: center;
    font-size: 25px;
    //line-height: 97px;
    font-weight: 700;
    top: 10%;
    left: 10%;
    z-index: 2;
    color: rgba(255, 255, 255, .7);
  }

  .team-swiper {
    ${tw`mb-10`} //&__swiper-button-prev {
            //  background-image: url("/images/svg/arrow-prev.svg");
            //  position: absolute;
            //  content: "";
            //  top: 150px;
            //  right: 88px;
            //  height: 90px;
            //  width: 90px;
            //  z-index: 3;
            //
            //  &:hover svg {
            //    opacity: .7;
            //  }
            //}
            //
            //&__swiper-button-next {
            //  background-image: url("/images/svg/arrow-next.svg");
            //  position: absolute;
            //  content: "";
            //  top: 150px;
            //  right: 0;
            //  height: 90px;
            //  width: 90px;
            //  z-index: 3;
            //
            //  &:hover {
            //    opacity: .7;
            //  }
            //}
  }
`
