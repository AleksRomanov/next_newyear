import { TeaserStyles } from './Teaser.styles.tw'
import { Button, Input } from 'antd'
import React from 'react'
import 'swiper/css'
import Image from 'next/image'

export default function Teaser(): JSX.Element {
  return (
    <TeaserStyles>
      <div className="teaser-image">
        <Image className={'teaser-picture'} src={'/images/teaser/helen.jpg'} alt={'teaser'} width={375} height={0}></Image>
      </div>
      <div className="teaser-wrapper">
        <div className="teaser-info">
          <h1>Профессиональный переводчик китайского</h1>
          <div className={'teaser-info__text'}>
            <p>Принимаю заказы на перевод любой литературы, документов, комиксов, журналов, книг, видеоматериалов напрямую и любой сложности</p>
          </div>
          <Input.Group className={'teaser-input'} compact>
            <Input style={{ width: 'calc(100% - 100px)', height: '40px', fontSize: '10px' }} placeholder="Напишите мне, отвечу в течении 10 минут" />
            <Button className={'input-button'} type="primary">
              Submit
            </Button>
          </Input.Group>
        </div>
      </div>
    </TeaserStyles>
  )
}
