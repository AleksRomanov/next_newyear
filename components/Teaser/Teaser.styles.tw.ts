import tw from "twin.macro";
import styled from "styled-components";

export const TeaserStyles = styled.div`
  ${tw`relative text-white text-center lg:w-[1250px] mb-[97px] md:mb-[150px] lg:mb-[150px] m-[0 auto] lg:flex lg:flex-row lg:py-[70px] md:py-[40px] lg:justify-center lg:justify-around`},
  .teaser-wrapper {
    ${tw``}
  }
.teaser-image {
  ${tw`lg:order-2`}
}
  .teaser-info {
    position: relative;
    box-sizing: border-box; 
    padding: 20px 0 0 0;
    ${tw`lg:mt-[10px] lg:w-[560px] z-10 text-center`}
    &__text {
      display: block;
      margin: 0 auto;
      margin-bottom: 64px;
      background-color: rgba(1, 1, 1, .45);
      width: 322px;
      height: 93px;
      border-radius: 8px;
    }
  }

  .teaser-picture {
    ${tw`lg:block lg:static lg:rounded-[50px] md:w-[350px]`}
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
    text-align: center;
    //border-radius: 20px;
    //width: 90%;
    //min-height: 504px;
    z-index: 0;
    ${tw``}
  }

  .teaser-input {
    ${tw`md:w-3/5 md:m-[0 auto]`}
    top: 12%;
    width: 100%;
    //min-width: 235px;
    ${tw``}
  }
  

  .input-button {
    ${tw`h-[40px] text-black bg-amber-400 hover:bg-amber-700`}
  }

  p {}

  h1 {
    ${tw`md:mb-[30px] md:text-[52px] font-bold lg:leading-[97px] md:text-5xl`}
    margin-bottom: 180px;
    font-size: 25px;
    //line-height: 97px;
    //font-weight: 700;
    //top: 10%;
    //left: 10%;
    text-align: center;
    z-index: 2;
    color: rgba(255, 255, 255, .7);
  }
`
