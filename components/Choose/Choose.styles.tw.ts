import tw from "twin.macro";
import styled from "styled-components";

export const ChooseStyles = styled.div`
  ${tw`relative flex flex-wrap justify-center items-center px-[20px] m-[0 auto] mb-[70px] lg:w-[1250px]`},
  .choose-title {
    ${tw`lg:mb-[100px] md:text-5xl md:mb-[100px]`}
    margin-bottom: 20px;
  }

  .swiper-item__title {
    margin-bottom: 0;
  }

  .choose-swiper {
    width: 100%;

    ${tw`mb-10`}
    .swiper-item__title {
      margin-bottom: 20px;
    }

    //&__swiper-button-prev {
    //  background-image: url("/images/svg/arrow-prev.svg");
    //  position: absolute;
    //  content: "";
    //  top: 150px;
    //  right: 88px;
    //  height: 90px;
    //  width: 90px;
    //  z-index: 3;
    //
    //  &:hover svg {
    //    opacity: .7;
    //  }
    //}

    //&__swiper-button-next {
    //  background-image: url("/images/svg/arrow-next.svg");
    //  position: absolute;
    //  content: "";
    //  top: 150px;
    //  right: 0;
    //  height: 90px;
    //  width: 90px;
    //  z-index: 3;
    //
    //  &:hover {
    //    opacity: .7;
    //  }
    //}
  }
  h1 {
    ${tw`lg:text-6xl`}
    margin-bottom: 170px;
    font-size: 25px;
    text-align: center;
    //line-height: 77px;
    font-weight: 600;
    z-index: 2;
    color: rgba(255, 255, 255, .7);
  }

  .choose-list {
    ${tw``}
    p {
      ${tw`mb-12`}
    }

    &__item {
      ${tw` text-white min-h-[377px] hover:bg-stone-800  text-xl`}
      background-color: rgba(29, 29, 29, .1);
      border: 1.4px solid rgba(255, 255, 255, 0.1);
      border-radius: 14px;
      margin-right: 0;
    }

    &__item-btn {
      ${tw`block m-[0 auto] bg-amber-400 hover:bg-amber-700 text-black rounded-2xl w-[121px] h-[43px]`}
    }
  }

  .ant-card .ant-card-head {
    color: #ffffff;
    text-align: center;
  }

  .swiper-backface-hidden .swiper-slide {
    display: flex;
    justify-content: center;
  }

  //.swiper-backface-hidden .swiper-slide {
  //  display: flex;
  //  flex-wrap: wrap;
  //  align-items: center;
  //  justify-content: center;
  //  text-align: center;
  //}

 
`
