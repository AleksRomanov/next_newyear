import React from 'react'
import { ChooseStyles } from './Choose.styles.tw'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Button, Card } from 'antd'

export default function Choose(): JSX.Element {
  return (
    <ChooseStyles className={'choose'}>
      <h1 className={'choose-title'}>Почему стоит выбрать именно меня?</h1>
      <div className="choose-swiper">
        {/* eslint-disable-next-line no-console */}
        <Swiper
          className={'choose-list'}
          spaceBetween={0}
          slidesPerView={1}
          onSlideChange={() => console.log('slide change')}
          onSwiper={(swiper) => console.log(swiper)}
          breakpoints={{
            768: {
              // width: 768,
              spaceBetween: 30,
              slidesPerView: 2,
            },
            // when window width is >= 640px
            1250: {
              // width: 336,
              spaceBetween: 30,
              slidesPerView: 3,
            },
            // when window width is >= 768px
          }}
        >
          <SwiperSlide>
            <Card className={'choose-list__item'} title="Программа 'Для студентов'" bordered={false}>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ducimus incidunt iste tempora velit. A, adipisci atque autem culpa mollitia nemo omnis quaerat quos voluptatibus. At, quasi, voluptatum! In, sint?</p>
              <Button className={'choose-list__item-btn'} type="primary">
                Подробнее
              </Button>
            </Card>
          </SwiperSlide>
          <SwiperSlide>
            <Card className={'choose-list__item'} title="Программа 'Перевод документации'" bordered={true}>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ducimus incidunt iste tempora velit. A, adipisci atque autem culpa mollitia nemo omnis quaerat quos voluptatibus. At, quasi, voluptatum! In, sint?</p>
              <Button className={'choose-list__item-btn'} type="primary">
                Подробнее
              </Button>
            </Card>
          </SwiperSlide>
          <SwiperSlide>
            <Card className={'choose-list__item'} title="Язык для себя" bordered={true}>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis ducimus incidunt iste tempora velit. A, adipisci atque autem culpa mollitia nemo omnis quaerat quos voluptatibus. At, quasi, voluptatum! In, sint?</p>
              <Button className={'choose-list__item-btn'} type="primary">
                Подробнее
              </Button>
            </Card>
          </SwiperSlide>
        </Swiper>
      </div>
    </ChooseStyles>
  )
}
