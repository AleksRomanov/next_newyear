import styled from "styled-components";
import tw from "twin.macro";

export const HeaderLinksStyled = styled.nav`
  ${tw`hidden lg:flex`}

  .header-links {
    ${tw`lg:flex`}
  }
  .header-link {
    ${tw`flex mr-10`}
  }
`
