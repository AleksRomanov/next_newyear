import Link from 'next/link'
import { HeaderLinksStyled } from './HeaderLinks.styles.tw'

export default function HeaderLinks(): JSX.Element {
  return (
    <HeaderLinksStyled>
      <div className="header-links">
        <Link className={'header-link'} href={'/'}>
          Главная
        </Link>
        <Link className={'header-link'} href={'/about'}>
          Обо мне
        </Link>
        <Link href={'/posts'}>Мои работы</Link>
      </div>
    </HeaderLinksStyled>
  )
}
