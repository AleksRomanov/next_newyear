import { HeaderStyles } from './Header.styles.tw'
import HeaderNav from './HeaderNav/HeaderNav'
import Logo from '../../../public/images/svg/logo.svg'
import { useState } from 'react'
import { Button, Drawer } from 'antd'
import HeaderLinks from './HeaderLinks/HeaderLinks'
import Popup from '../Popup/Popup'

export default function Header(): JSX.Element {
  const [open, setOpen] = useState(false)

  const showDrawer = () => {
    setOpen(true)
  }
  const onClose = () => {
    setOpen(false)
  }
  return (
    <HeaderStyles className={'header'}>
      <div style={{ width: '140px' }}>
        <Logo className={'logo'} />
      </div>
      <HeaderLinks />
      <Popup />
      <Drawer width={300} title="Basic Drawer" placement="left" onClose={onClose} open={open}>
        <HeaderNav />
      </Drawer>
      <Button className={'header__burger-button'} type="primary" onClick={showDrawer}>
        Open
      </Button>
    </HeaderStyles>
  )
}
