import styled from "styled-components";
import tw from "twin.macro";

export const HeaderNavStyled = styled.nav`
  ${tw``}

  // .header-nav-links {
  //   ${tw``}
  // }
  .header-nav-link {
    ${tw`flex flex-col mr-10`}
  }

  // .header-nav__button {
  //   ${tw``}
  // }
`
