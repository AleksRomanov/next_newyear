import Link from 'next/link'
import { HeaderNavStyled } from './HeaderNav.styles.tw'
import { Menu, MenuProps } from 'antd'
import { useState } from 'react'

export default function HeaderNav(): JSX.Element {
  const [current, setCurrent] = useState('mail')

  const onClick: MenuProps['onClick'] = (e) => {
    console.log('click ', e)
    setCurrent(e.key)
  }
  const items: MenuProps['items'] = [
    {
      label: (
        <Link className={'header-nav-link'} href={'/'}>
          Главная
        </Link>
      ),
      key: 'mail',
      // icon: <MailOutlined />,
    },
    {
      label: (
        <Link className={'header-nav-link'} href={'/about'}>
          Обо мне
        </Link>
      ),
      key: 'app',
      // icon: <AppstoreOutlined />,
      // disabled: true,
    },
    {
      label: (
        <Link className={'header-nav-link'} href={'/posts'}>
          Мои работы
        </Link>
      ),
      key: 'app',
      // icon: <AppstoreOutlined />,
      // disabled: true,
    },
  ]
  return (
    <HeaderNavStyled>
      <Menu className="header-nav-links" onClick={onClick} selectedKeys={[current]} mode="vertical" items={items} />
    </HeaderNavStyled>
  )
}
