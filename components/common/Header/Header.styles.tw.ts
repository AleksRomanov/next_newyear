import styled from "styled-components";
import tw from "twin.macro";

export const HeaderStyles = styled.div`
  ${tw`relative h-[100px] flex items-center justify-between px-[20px] lg:px-[100px] border-b-2 `}
  .header {
    ${tw`lg:flex `}
    &__burger-button {
      ${tw`lg:hidden border-inherit bg-amber-400 hover:bg-amber-700 border-0 hover:text-white text-black`}
    }
  }
  .logo {
    ${tw`md:w-[80px] md:h-[80px]`}
  }


  background-color: rgba(29, 29, 29, .6);
  //position: fixed;
  //top: 0; 
  //right: 0;
  //left: 0;
  //z-index: 3;

  .logo_svg__icon {
    ${tw``}
  }



`
