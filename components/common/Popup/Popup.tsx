import React, { useState } from 'react'
import { PopupStyles } from './Popup.styles.tw'
import { Button, Cascader, Form, Input, Modal } from 'antd'
import axios from 'axios'

export default function Popup(): JSX.Element {
  const [form] = Form.useForm()

  async function sendForm() {
    try {
      await axios.post('http://localhost:3000/api/send-request', {
        name,
        surName,
        email,
        theme,
        message,
      })
      clearForm()
      setIsModalOpen(false)
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('Sending error', error)
    }
  }

  const [name, setName] = useState('')
  const [surName, setSurName] = useState('')
  const [email, setMail] = useState('')
  const [theme, setTheme] = useState('')
  const [message, setMessage] = useState('')
  const [isModalOpen, setIsModalOpen] = useState(false)
  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleOk = () => {
    setIsModalOpen(false)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
  }
  const clearForm = () => {
    form.resetFields()
  }

  const onFinish = (values: any) => {
    // eslint-disable-next-line no-console
    console.log('Success:', values)
  }

  const onFinishFailed = (errorInfo: any) => {
    // eslint-disable-next-line no-console
    console.log('Failed:', errorInfo)
  }

  interface Option {
    value: string | number
    label: string
  }

  const options: Option[] = [
    {
      value: 'Юридический',
      label: 'Юридический',
    },
    {
      value: 'Художественный',
      label: 'Художественный',
    },
    {
      value: 'Миграционные документы',
      label: 'Миграционные документы',
    },
    {
      value: 'Технический',
      label: 'Технический',
    },
    {
      value: 'Перевод субтитров',
      label: 'Перевод субтитров',
    },
  ]

  const onChange = (value) => {
    setTheme(value[0])
    // eslint-disable-next-line no-console
    console.log('Тема перевода - ', value)
  }

  return (
    <PopupStyles className={'popup'}>
      <Modal className="popup-modal" title="Форма для связи" footer={null} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <Form labelCol={{ span: 4 }} wrapperCol={{ span: 16 }} style={{ maxWidth: 600 }} onFinish={onFinish} onFinishFailed={onFinishFailed} form={form} autoComplete="on">
          <Form.Item label="Имя" name="name" rules={[{ required: true, message: 'Введите ваше имя' }]}>
            <Input placeholder="Ваше имя" value={name} onChange={(event) => setName(event.target.value)} />
          </Form.Item>
          <Form.Item label="Фамилия" name="surName" rules={[{ required: true, message: 'Введите вашу фамилию' }]}>
            <Input placeholder="Ваша фамилия" value={surName} onChange={(event) => setSurName(event.target.value)} />
          </Form.Item>
          <Form.Item label="Почта" name="email" rules={[{ required: true, message: 'Введите вашу почту' }]}>
            <Input placeholder="Ваша почта" value={email} onChange={(event) => setMail(event.target.value)} />
          </Form.Item>
          <Form.Item label="Тематика" name="theme" rules={[{ required: true, message: 'Выберите тематику' }]}>
            <Cascader options={options} onChange={onChange} placeholder="Выберите тематику перевода" />
          </Form.Item>
          <Form.Item label="Текст" name="message" rules={[{ required: true, message: 'Введите ваше сообщение' }]}>
            <Input.TextArea placeholder="Текс обращения" rows={4} maxLength={90} value={message} onChange={(event) => setMessage(event.target.value)} />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button onClick={sendForm}>Submit</Button>
            <Button onClick={clearForm}>Clear</Button>
          </Form.Item>
        </Form>
      </Modal>
      <div className="popup-contact">
        <Button onClick={showModal} className={'popup-contact__button'}>
          Связаться сейчас
        </Button>
      </div>
    </PopupStyles>
  )
}
