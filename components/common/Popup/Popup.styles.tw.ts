import tw from 'twin.macro'
import styled from 'styled-components'


export const PopupStyles = styled.div`
  ${tw``}
}
  .popup-contact {
    ${tw`hidden lg:block `}
    .popup-contact__button {
      ${tw`text-white bg-amber-400 hover:bg-amber-700 text-black border-0 hover:text-white`}
    }

`
