import styled from 'styled-components';
import tw from 'twin.macro';

export const FooterStyled = styled.div`
  ${tw`relative w-full flex justify-between items-center lg:px-[100px] px-[20px] h-[100px] border-t-2 border-white `}
  .footer-rights {
    ${tw`hidden lg:block `}
  }
  // .footer-link {
  //   ${tw`w-full flex justify-center`}
  // }
  .footer-button {
    ${tw`text-white hover:bg-blue-300 hover:text-black mx-10`}
  }
  .footer-logo {
    ${tw`w-[60px] block left-0 md:w-[80px] md:h-[80px]`}
  }
`
