import { FooterStyled } from './Footer.styles.tw'
// import { Button } from 'antd'
// import Router from 'next/router'
import React from 'react'
import Logo from '../../../public/images/svg/logo.svg'
import FooterNav from './FooterNav/FooterNav'
import Instagram from '../../../public/images/svg/instagram.svg'
import Vk from '../../../public/images/svg/vk.svg'

export default function Footer(): JSX.Element {
  // const linkClickHandler = () => {
  //   Router.push('/')
  // }
  return (
    <FooterStyled>
      <Instagram className={'footer-society__link'} />
      <Vk className={'footer-society__link'} />
      <FooterNav />
      <Logo className={'footer-logo'} />
      {/*<div className="footer-rights">*/}
      {/*  <p>© 2023. Все права защищены</p>*/}
      {/*</div>*/}
    </FooterStyled>
  )
}
