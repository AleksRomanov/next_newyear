import Link from 'next/link'
import { FooterNavStyled } from './FooterNav.styles.tw'
import Instagram from '../../../../public/images/svg/instagram.svg'
import Vk from '../../../../public/images/svg/vk.svg'
import React from 'react'
// import { Button } from 'antd'

export default function FooterNav(): JSX.Element {
  return (
    <FooterNavStyled>
      <div className="footer-society"></div>
      <div className="footer-nav-links">
        <Link className={'footer-nav-link'} href={'/'}>
          Главная
        </Link>
        <Link className={'footer-nav-link'} href={'/about'}>
          Обо мне
        </Link>
        <Link className={'footer-nav-link'} href={'/posts'}>
          Мои работы
        </Link>
      </div>
      {/*<div className="footer-nav-contact">*/}
      {/*  <Button className={'footer-nav__button'}>Связаться сейчас</Button>*/}
      {/*</div>*/}
    </FooterNavStyled>
  )
}
