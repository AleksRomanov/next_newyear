import styled from "styled-components";
import tw from "twin.macro";

export const FooterNavStyled = styled.nav`
  ${tw`flex items-center m-[0 auto] justify-between bg-transparent flex text-white`}
  .footer-nav-link {
    ${tw`mr-3 text-[10px] lg:text-[15px] md:text-base`}
  }

  .footer-nav__button {
    ${tw`text-white m-[auto] bg-amber-400 hover:bg-amber-700 text-black border-0 hover:text-white`}
  }
  .footer-society {
    ${tw`hidden lg:block `}
    display: flex;
    //position: absolute;
    //left: 0;
    &__link {
      width: 30px;
      height: 30px;
      margin-right: 13px;
    }
  }

`
