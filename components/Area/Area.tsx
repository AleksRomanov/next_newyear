import { AreaStyles } from './Area.styles.tw'
import React from 'react'
import 'swiper/css'
import Image from 'next/image'
import { Swiper, SwiperSlide } from 'swiper/react'

export default function Area(): JSX.Element {
  return (
    <AreaStyles className={'area'}>
      <h1>Области перевода и тематики</h1>
      <div className="area-swiper">
        <Swiper
          className={'area-list'}
          spaceBetween={0}
          slidesPerView={1}
          onSlideChange={() => console.log('slide change')}
          onSwiper={(swiper) => console.log(swiper)}
          breakpoints={{
            768: {
              spaceBetween: 30,
              slidesPerView: 2,
            },
            1250: {
              spaceBetween: 30,
              slidesPerView: 3,
            },
          }}
        >
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/1.jpg'} alt={'Slide 1'} width={368} height={305} />
            <span className={'area-list__item-title'}>Повседневные переводы</span>
          </SwiperSlide>
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/2.jpg'} alt={'Slide 2'} width={368} height={305} />
            <span className={'area-list__item-title'}>Промышленные тематики</span>
          </SwiperSlide>
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/3.jpg'} alt={'Slide 3'} width={368} height={305} />
            <span className={'area-list__item-title'}>Общение с Китайскими фирмами</span>
          </SwiperSlide>
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/4.jpg'} alt={'Slide 4'} width={368} height={305} />
            <span className={'area-list__item-title'}>Промышленные тематики</span>
          </SwiperSlide>
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/5.jpg'} alt={'Slide 5'} width={368} height={305} />
            <span className={'area-list__item-title'}>Промышленные тематики</span>
          </SwiperSlide>
          <SwiperSlide className={'area-list__item'}>
            <Image src={'/images/area/6.jpg'} alt={'Slide 6'} width={368} height={305} />
            <span className={'area-list__item-title'}>Промышленные тематики</span>
          </SwiperSlide>
        </Swiper>
      </div>

      {/*<div className="area-list">*/}
      {/*  <div className="area-list__item" style={{ width: 752, marginRight: 30 }}>*/}
      {/*    <Image src={'/images/area/1.jpg'} alt={'area'} width={752} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Повседневные переводы</span>*/}
      {/*  </div>*/}
      {/*  <div className="area-list__item" style={{ width: 358 }}>*/}
      {/*    <Image src={'/images/area/2.jpg'} alt={'area'} width={358} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Промышленные тематики</span>*/}
      {/*  </div>*/}
      {/*  <div className="area-list__item" style={{ width: 358, marginRight: 30 }}>*/}
      {/*    <Image src={'/images/area/3.jpg'} alt={'area'} width={358} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Общение с Китайскими фирмами</span>*/}
      {/*  </div>*/}
      {/*  <div className="area-list__item" style={{ width: 752 }}>*/}
      {/*    <Image src={'/images/area/4.jpg'} alt={'area'} width={752} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Lorem ipsum dolor sit amet</span>*/}
      {/*  </div>*/}
      {/*  <div className="area-list__item" style={{ width: 752, marginRight: 30 }}>*/}
      {/*    <Image src={'/images/area/5.jpg'} alt={'area'} width={752} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Lorem ipsum dolor sit amet</span>*/}
      {/*  </div>*/}
      {/*  <div className="area-list__item" style={{ width: 358 }}>*/}
      {/*    <Image src={'/images/area/6.jpg'} alt={'area'} width={358} height={342} />*/}
      {/*    <span className={'area-list__item-title'}>Lorem ipsum dolor sit amet</span>*/}
      {/*  </div>*/}
      {/*</div>*/}
    </AreaStyles>
  )
}
