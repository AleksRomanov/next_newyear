import tw from 'twin.macro'
import styled from 'styled-components'

export const AreaStyles = styled.div`
  ${tw`relative block m-[0 auto] text-center px-[20px] lg:mb-[200px] md:mb-[100px]`}
  h1 {
    ${tw`lg:text-6xl lg:mb-[100px] md:text-5xl md:mb-[100px]`}
    margin-bottom: 30px;
    font-size: 25px;
    //line-height: 77px;
    font-weight: 600;
    z-index: 2;
    color: rgba(255, 255, 255, .7);
  }

  .area-list {
    ${tw`flex flex-wrap justify-center`}
    &__item {
      ${tw`relative text-white h-[305px] mb-[30px] hover:opacity-80 rounded-2xl text-xl`}
      background-color: rgba(29, 29, 29, .1);
      border: 1.4px solid rgba(255, 255, 255, 0.1);
      border-radius: 14px;
    }
    
    &__item-title {
      ${tw`absolute bottom-36 right-0 left-0`}
    }
  }
  .area-swiper {
    ${tw`mb-10 `}
    .swiper-item__title {
      margin-bottom: 20px;
    }

    //&__swiper-button-prev {
    //  background-image: url("/images/svg/arrow-prev.svg");
    //  position: absolute;
    //  content: "";
    //  top: 150px;
    //  right: 88px;
    //  height: 90px;
    //  width: 90px;
    //  z-index: 3;
    //
    //  &:hover svg {
    //    opacity: .7;
    //  }
    //}

    //&__swiper-button-next {
    //  background-image: url("/images/svg/arrow-next.svg");
    //  position: absolute;
    //  content: "";
    //  top: 150px;
    //  right: 0;
    //  height: 90px;
    //  width: 90px;
    //  z-index: 3;
    //
    //  &:hover {
    //    opacity: .7;
    //  }
    //}
  }

  .swiper-backface-hidden .swiper-slide {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
`
