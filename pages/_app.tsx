import '../styles/main.module.css'
import NextNProgress from 'nextjs-progressbar'
import '../styles/globals.css'
import localFont from '@next/font/local'
import { ConfigProvider } from 'antd'

const montserratFont = localFont({
  src: '../public/fonts/Montserrat-Regular.woff2',
  variable: '--font-inter',
  weight: 'normal',
  style: 'normal',
})

export default function MyApp({ Component, pageProps }) {
  return (
    <ConfigProvider theme={{ hashed: false }}>
      <div className={`${montserratFont.variable} font-sans`}>
        <NextNProgress color="red" startPosition={0.3} stopDelayMs={200} height={3} showOnShallow={true} />
        <Component {...pageProps} />
      </div>
    </ConfigProvider>
  )
}
