import { Html, Head, Main, NextScript, DocumentContext } from 'next/document'
import { stylesSSR } from '../lib/StylesSSR'

export default function Document() {
  return (
    <Html>
      <Head></Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

Document.getInitialProps = async (ctx: DocumentContext) => {
  const initialProps = await stylesSSR(ctx)
  return { ...initialProps }
}
