import MainLayout from '../components/MainLayout'
import Teaser from '../components/Teaser/Teaser'
import Introduce from '../components/Introduce/Introduce'
import Choose from '../components/Choose/Choose'
import Area from '../components/Area/Area'
import Reviews from '../components/Reviews/Reviews'
import Team from '../components/Team/Team'

export default function Index() {
  return (
    <MainLayout title={'Переводчик Елена'}>
      <Teaser />
      <Introduce />
      <Choose />
      <Area />
      <Reviews />
      <Team />
    </MainLayout>
  )
}
