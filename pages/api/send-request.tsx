import sendEmail from '../../lib/mail'

export default async function handler(req, res) {
  const message = {
    to: 'er.translate@mail.ru',
    subject: `Письмо с сайта Елены Романовой от ${req.body.name}`,
    text: `
    Имя: ${req.body.name},
    Фамилия: ${req.body.surName},
    Почта: ${req.body.email},
    Тематика: ${req.body.theme},
    Сообщение: ${req.body.message},
    `,
  }
  sendEmail(message)
  // eslint-disable-next-line no-console
  console.log(message)
  res.send(`Спасибо за заявку, ${req.body.name}!`)
}
