import Link from "next/link";
import classes from "../styles/error.module.css"

export default function ErrorPage() {
    return (
        <>
            <div className={classes.wrapper}>
                <h1 className={classes.error}>ERROR 404!!!</h1>
                <p>Please go to the <Link href={'/'}>Home page</Link></p>
            </div>
        </>
    )
}
