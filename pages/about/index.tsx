// import Router from 'next/router'
import MainLayout from '../../components/MainLayout'
import About from '../../components/About/About'

export default function AboutIndex() {
  return (
    <MainLayout title={'VASSAL - О нас'}>
      <About title={'VERCEL'} />
    </MainLayout>
  )
}

AboutIndex.getInitialProps = async () => {
  const response = await fetch(`${process.env.API_URL}/about`)
  const data = await response.json()

  return {
    title: data.title,
  }
}
