import {useRouter} from "next/router";
import MainLayout from "../../components/MainLayout";
// import Posts from "@/pages/posts";
import Link from "next/link";
import {useEffect, useState} from "react";
import {NextPageContext} from "next";
import {MyPost} from "../../interfaces/post";

interface PostPageProps {
    post: MyPost
}

export default function Post({post: serverPost}: PostPageProps) {
    const [post, setPost] = useState(serverPost)
    const router = useRouter()

    useEffect(() => {
        async function load() {
            const response = await fetch(`${process.env.API_URL}/posts/${router.query.id}`)
            const data = await response.json()
            setPost(data)
        }

        if (!serverPost) {
            load()
        }
    }, [])

    if (!post) {
        return <MainLayout>
            <p>lOADING . . . </p>
        </MainLayout>
    }

    return (
        <MainLayout title={'Post page!'}>
            <h1>{post.title}</h1>
            <hr/>
            <p>{post.body}</p>
            <Link href={'/posts'}>Back to all Posts</Link>
            {/*<h1>Post {router.query.id}</h1>*/}
        </MainLayout>
    )
}

interface PostNextPageContext extends NextPageContext {
    query: {
        id: string
    }
}

Post.getInitialProps = async ({query, req}: PostNextPageContext) => {
    if (!req) {
        return {post: null}
    }

    const response = await fetch(`http://localhost:4200/posts/${query.id}`)
    const post: MyPost = await response.json()
    return {
        post
    }
}

// export async function getServerSideProps({query, req}) {
//     if (!req) {
//         return {post: null}
//     }
//     const response = await fetch(`http://localhost:4200/posts/${query.id}`)
//     const post = await response.json()
//     return {
//        props: {post}
//     }
// }

