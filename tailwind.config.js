/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')
// import defaultTheme from 'tailwindcss/defaultTheme';

module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['var(--font-inter)', ...defaultTheme.fontFamily.sans],
        // serif: ['var(--font-number)'],
      },
      colors: {
        blue: '#00408C',
        'blue-300': '#5983b4',
        'blue-600': '#82B6F4',
        'dark-blue': '#262a3b',
        purple: '#561e37',
        pink: '#ff49db',
        orange: '#ff7849',
        olive: '#717845',
        green: '#2f6162',
        'acid-green': '#52C41A',
        yellow: '#ffc82c',
        'gray-dark': '#232934',
        gray: '#8492a6',
        'gray-light': '#d3dce6',
      },
      screens: {},
    },
  },
  plugins: [],
}
