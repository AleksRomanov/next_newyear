const withTwin = require('./lib/withTwin.js');

module.exports = withTwin({
  env: {
    API_URL: process.env.API_URL
  },
  // compiler: {
  //   styledComponents: true,
  // },

  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: { and: [/\.(js|ts)x?$/] },
      use: ['@svgr/webpack']
    },);
    config.resolve.fallback = { fs: false };


    return config;
  },
});


