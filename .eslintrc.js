module.exports = {
  env: {
    es2019: true,
    browser: true,
    node: true,
  },
  extends: ['plugin:@typescript-eslint/recommended'],
  plugins: [
    'babel',
    'import',
    'jsx-a11y',
    'react',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 10,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      tsx: true,
    }
  },
  rules: {
    'arrow-body-style': 'off', // Это - не наш стиль?
    'arrow-parens': 'off', // Несовместимо с prettier
    'function-paren-newline': 'off', // Несовместимо с prettier
    'jsx-a11y/anchor-is-valid': ['error', {'components': ['Link'], 'specialLink': ['hrefLeft', 'hrefRight'], 'aspects': ['invalidHref', 'preferButton'] }],

    'jsx-a11y/label-has-for': [2, {
      'required': {
        'every': ['id']
      }
    }], // для ошибки вложенных свойств htmlFor элементов label
    'linebreak-style': 'off', // Неправильно работает в Windows.
    // 'max-len': [ 'error', 100, 2, { ignoreUrls: true, } ], // airbnb позволяет некоторые пограничные случаи
    'no-console': 'error', // airbnb использует предупреждение
    'no-alert': 'error', // airbnb использует предупреждение
    'no-mixed-operators': 'off', // Несовместимо с prettier
    'no-param-reassign': 'off', // Это - не наш стиль?
    '@typescript-eslint/no-var-requires': 'off',
    'no-plusplus': 'off',
    'object-curly-newline': 'off', // Несовместимо с prettier
    // 'prefer-destructuring': 'off',
    'radix': 'off', // parseInt, parseFloat и radix выключены. Мне это не нравится.
    'react/forbid-prop-types': 'off', // airbnb использует уведомление об ошибке
    'react/jsx-filename-extension': ['error', {extensions: ['.js', '.tsx']}], // airbnb использует .jsx
    'react/jsx-one-expression-per-line': 'off',
    'react/no-find-dom-node': 'off', // Я этого не знаю
    'react/no-did-mount-set-state': 'off',
    'react/no-unused-prop-types': 'off', // Это всё ещё работает нестабильно
    // 'react/require-default-props': 'off', // airbnb использует уведомление об ошибке
    'space-before-function-paren': 0, // Несовместимо с prettier
    '@typescript-eslint/no-unused-vars': 'error',
    'no-unused-vars': 'off',
    // 'prettier/prettier': [ 'error' ],
  },
};
