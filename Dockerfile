FROM node:16-alpine

WORKDIR /app

COPY package.json package.json.lock
RUN npm install

COPY pages ./pages
COPY public ./public
COPY styles ./styles

CMD ["npm", "dev"]
